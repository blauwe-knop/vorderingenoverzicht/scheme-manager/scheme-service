// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

type OrganizationRepository interface {
	Create(ctx context.Context, newOrganizationSetting model.Organization) (*model.Organization, error)
	List(ctx context.Context) (*[]model.Organization, error)
	Get(ctx context.Context, id string) (*model.Organization, error)
	Update(ctx context.Context, newOrganizationSetting model.Organization, oin string) error
	Delete(ctx context.Context, oin string) error
	healthcheck.Checker
}
