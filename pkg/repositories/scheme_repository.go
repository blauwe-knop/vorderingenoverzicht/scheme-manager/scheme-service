// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

type SchemeRepository interface {
	ListOrganizations() (*[]model.Organization, error)
	CreateOrganization(model.Organization) (*model.Organization, error)
	GetOrganization(oin string) (*model.Organization, error)
	UpdateOrganization(oin string, organization model.Organization) (*model.Organization, error)
	DeleteOrganization(oin string) (*model.Organization, error)
	healthcheck.Checker
}
