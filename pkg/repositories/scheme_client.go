// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

type SchemeClient struct {
	baseURL string
	apiKey  string
}

var ErrOrganizationNotFound = errors.New("organization does not exist")

func NewSchemeClient(baseURL string, apiKey string) *SchemeClient {
	return &SchemeClient{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *SchemeClient) ListOrganizations() (*[]model.Organization, error) {
	url := fmt.Sprintf("%s/organizations", s.baseURL)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	organizations := []model.Organization{}
	err = json.Unmarshal(body, &organizations)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &organizations, nil
}

func (s *SchemeClient) CreateOrganization(organization model.Organization) (*model.Organization, error) {
	url := fmt.Sprintf("%s/organizations", s.baseURL)

	requestBodyAsJson, err := json.Marshal(organization)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to post organization request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organization: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organization: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnOrganization := model.Organization{}
	err = json.Unmarshal(body, &returnOrganization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnOrganization, nil
}

func (s *SchemeClient) GetOrganization(oin string) (*model.Organization, error) {
	url := fmt.Sprintf("%s/organizations/%s", s.baseURL, oin)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrOrganizationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d, oin: %s", resp.StatusCode, oin)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	organization := model.Organization{}
	err = json.Unmarshal(body, &organization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, oin: %s: %v", oin, err)
	}

	return &organization, nil
}

func (s *SchemeClient) UpdateOrganization(oin string, organization model.Organization) (*model.Organization, error) {
	url := fmt.Sprintf("%s/organizations/%s", s.baseURL, oin)

	requestBodyAsJson, err := json.Marshal(organization)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to post organizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnOrganization := model.Organization{}
	err = json.Unmarshal(body, &returnOrganization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnOrganization, nil
}

func (s *SchemeClient) DeleteOrganization(oin string) (*model.Organization, error) {
	url := fmt.Sprintf("%s/organizations/%s", s.baseURL, oin)

	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to post organizations request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving organizations: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	organization := model.Organization{}
	err = json.Unmarshal(body, &organization)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &organization, nil
}

func (s *SchemeClient) GetHealthCheck() healthcheck.Result {
	name := "scheme-service"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
