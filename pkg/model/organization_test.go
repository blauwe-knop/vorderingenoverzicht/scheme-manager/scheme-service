package model_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service/pkg/model"
)

func TestOrganizationModel(t *testing.T) {
	organization := model.Organization{
		Oin:          "00000000000000000001",
		Name:         "mock-org 1",
		DiscoveryUrl: "http://mock1/v1",
		PublicKey: `-----BEGIN PUBLIC KEY-----
		MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxlibRfmDYIrD9MJRh0BS
		utHKiYlXeGopRHT2G8FK8WGvN2faR1ZFdWjTxgncmb1nds7OoG5u2E1KrDMiZ3SH
		M7T95Ynle1LHE9D0VhUXA0iAYOq4aumHiyHpmT8XQhRsF/3RjGoGagtnaeQ3uSyd
		NBkOiuVm/JTmcU3QDhLwj8st/GvxwaViuLsIH4WNX/LmkPkVbW5/CLNtD9mTmMgt
		x3tEBYHyIctPoBrXfYBaJN4tv6JY1vI37cafG4MBzunAr4iP/1GBhh6ql8bpdqd7
		cD6cHvGmlubrmdILm9V0erNDV9O9NrjxIalYRU/5E10qGIAuBdBubfOvJQX8QgFa
		WQIDAQAB
		-----END PUBLIC KEY-----`,
		Approved: true,
		LogoUrl:  "https://mock1/logo.svg",
	}

	assert.NotNil(t, organization)
}
