package events

var (
	SCMS_1 = NewEvent(
		"scms_1",
		"started listening",
		Low,
	)
	SCMS_2 = NewEvent(
		"scms_2",
		"server closed",
		High,
	)
	SCMS_3 = NewEvent(
		"scms_3",
		"error creating organization db",
		VeryHigh,
	)
	SCMS_4 = NewEvent(
		"scms_4",
		"received request for openapi spec as JSON",
		Low,
	)
	SCMS_5 = NewEvent(
		"scms_5",
		"sent response with openapi spec as JSON",
		Low,
	)
	SCMS_6 = NewEvent(
		"scms_6",
		"received request for openapi spec as YAML",
		Low,
	)
	SCMS_7 = NewEvent(
		"scms_7",
		"sent response with openapi spec as YAML",
		Low,
	)
	SCMS_8 = NewEvent(
		"scms_8",
		"failed to read openapi.json file",
		High,
	)
	SCMS_9 = NewEvent(
		"scms_9",
		"failed to read openapi.yaml file",
		High,
	)
	SCMS_10 = NewEvent(
		"scms_10",
		"failed to write fileBytes to response",
		High,
	)
	SCMS_11 = NewEvent(
		"scms_11",
		"setting up connection to database",
		Low,
	)
	SCMS_12 = NewEvent(
		"scms_12",
		"successfully pinged database",
		Low,
	)
	SCMS_13 = NewEvent(
		"scms_13",
		"failed to open database connection",
		High,
	)
	SCMS_14 = NewEvent(
		"scms_14",
		"failed to ping database",
		High,
	)
	SCMS_15 = NewEvent(
		"scms_15",
		"failed to setup database connection",
		VeryHigh,
	)
	SCMS_16 = NewEvent(
		"scms_16",
		"failed to encode response payload",
		High,
	)
	SCMS_17 = NewEvent(
		"scms_17",
		"failed to decode request payload",
		High,
	)
	SCMS_18 = NewEvent(
		"scms_18",
		"received request for list of organization",
		Low,
	)
	SCMS_19 = NewEvent(
		"scms_19",
		"sent response with list of organization",
		Low,
	)
	SCMS_20 = NewEvent(
		"scms_20",
		"received request to create organization",
		Low,
	)
	SCMS_21 = NewEvent(
		"scms_21",
		"sent response with created organization",
		Low,
	)
	SCMS_22 = NewEvent(
		"scms_22",
		"received request to get organization",
		Low,
	)
	SCMS_23 = NewEvent(
		"scms_23",
		"sent response with organization",
		Low,
	)
	SCMS_24 = NewEvent(
		"scms_24",
		"received request to update organization",
		Low,
	)
	SCMS_25 = NewEvent(
		"scms_25",
		"sent response with updated organization",
		Low,
	)
	SCMS_26 = NewEvent(
		"scms_26",
		"received request to delete organization",
		Low,
	)
	SCMS_27 = NewEvent(
		"scms_27",
		"sent response with deleted organization",
		Low,
	)
	SCMS_28 = NewEvent(
		"scms_28",
		"failed to list organizations",
		High,
	)
	SCMS_29 = NewEvent(
		"scms_29",
		"failed to create organization",
		High,
	)
	SCMS_30 = NewEvent(
		"scms_30",
		"organization not found",
		High,
	)
	SCMS_31 = NewEvent(
		"scms_31",
		"failed to get organization",
		High,
	)
	SCMS_32 = NewEvent(
		"scms_32",
		"failed to update organization",
		High,
	)
	SCMS_33 = NewEvent(
		"scms_33",
		"failed to delete organization",
		High,
	)
)
